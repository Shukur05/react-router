import React from 'react';
import {BrowserRouter as Router, Switch, Link, Route} from "react-router-dom";
import './App.css'
import C1 from "./components/C1";
import C2 from "./components/C2";
import C3 from "./components/C3";
import C4 from "./components/C4";
import C5 from "./components/C5";
import C6 from "./components/C6";

const App = () => {
    return (
        <div>
            <Router>

             <div className="navigation">
                 <Link to={'/'}>Root</Link>
                 <Link to={'c1'}>Component 1</Link>
                 <Link to={'c2'}>Component 2</Link>
                 <Link to={'c3'}>Component 3</Link>
                 <Link to={'c4'}>Component 4</Link>
                 <Link to={'c5'}>Component 5</Link>
                 <Link to={'c6'}>Component 6</Link>
             </div>

                <Switch>

                    <Route path={'/'} exact render={() => (<p className={'components'}>Root</p>)}/>
                    <Route path={'/c1'} component={C1}/>
                    <Route path={'/c1'} component={C1}/>
                    <Route path={'/c2'} component={C2}/>
                    <Route path={'/c3'} component={C3}/>
                    <Route path={'/c4'} component={C4}/>
                    <Route path={'/c5'} component={C5}/>
                    <Route path={'/c6'} component={C6}/>

                </Switch>

            </Router>
        </div>
    );
};

export default App;